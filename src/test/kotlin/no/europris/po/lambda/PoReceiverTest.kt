package no.europris.po.app

import no.europris.po.lambda.PoReceiver
import org.junit.jupiter.api.Test
import java.time.LocalDate
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class PoReceiverTest {

    @Test
    fun should_generate_oms_file_name() {
        val fileName = PoReceiver.generateOMSFileName("1234567", LocalDate.parse("2020-12-01"))

        assertNotNull(fileName)
        assertTrue(
                fileName.matches(Regex("oms-1234567-01122020-[0-9a-f-]{36}\\.xml")),
                "Filename is not formatted correctly"
        )
    }

    @Test
    fun should_generate_asn_file_name() {
        val fileName = PoReceiver.generateASNFileName("8810", LocalDate.parse("2021-03-26"))

        assertNotNull(fileName)
        assertTrue(
                fileName.matches(Regex("asn-8810-26032021-[0-9a-f-]{36}\\.xml")),
                "Filename is not formatted correctly"
        )
    }

}