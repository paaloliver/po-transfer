package no.europris.po.pipeline

import no.europris.po.app.PoTransferStack
import software.amazon.awscdk.core.*
import software.amazon.awscdk.pipelines.CdkPipeline
import software.amazon.awscdk.pipelines.SimpleSynthAction
import software.amazon.awscdk.pipelines.SimpleSynthActionProps
import software.amazon.awscdk.services.codepipeline.Artifact
import software.amazon.awscdk.services.codepipeline.actions.BitBucketSourceAction
import software.amazon.awscdk.services.codepipeline.actions.ManualApprovalAction

class PoTransferPipelineStack @JvmOverloads constructor(scope: Construct?, id: String?, props: StackProps? = null) : Stack(scope, id, props) {
    init {
        val sourceArtifact = Artifact()
        val cloudAssemblyArtifact = Artifact()
        val pipeline = CdkPipeline.Builder.create(this, "Pipeline")
                .pipelineName("PoTransferPipeline")
                .cloudAssemblyArtifact(cloudAssemblyArtifact)
                .sourceAction(BitBucketSourceAction.Builder.create()
                        .actionName("Bitbucket")
                        .output(sourceArtifact)
                        .owner("paaloliver")
                        .repo("po-transfer")
                        .connectionArn("arn:aws:codestar-connections:us-east-1:477992598373:connection/f469588d-5d2d-4aff-9dd7-b18cc5e60acf")
                        .build())
                .synthAction(SimpleSynthAction(
                        SimpleSynthActionProps.builder()
                                .sourceArtifact(sourceArtifact)
                                .cloudAssemblyArtifact(cloudAssemblyArtifact)
                                .installCommands(listOf("npm install -g aws-cdk"))
                                .buildCommands(listOf("mvn clean package"))
                                .synthCommand("cdk synth")
                                .build()))
                .build()

        val testEnvironment = PoTransferApplicationStage(this, "TestEnvironment",
                StageProps.Builder().env(
                        Environment.Builder()
                                .account("477992598373") // sandbox TODO: parameterize ?
                                .region("eu-west-1")
                                .build()
                ).build())

        pipeline.addApplicationStage(testEnvironment)

        val manualApprovalStage = pipeline.addStage("Manual_approval_stage")

        manualApprovalStage.addActions(ManualApprovalAction.Builder.create()
                .actionName("Manual_approval_required")
                .runOrder(manualApprovalStage.nextSequentialRunOrder())
                .build())
    }
}

internal class PoTransferApplicationStage(scope: Construct?, id: String?, props: StageProps?) : Stage(scope!!, id!!, props) {
    init {
        val lambdaPath = "target/" + System.getProperty("lambda.jar")
        PoTransferStack(this, "PoTransfer", lambdaPath)
    }
}