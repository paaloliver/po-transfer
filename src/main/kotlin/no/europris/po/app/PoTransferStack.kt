package no.europris.po.app

import no.europris.po.lambda.helper.PoTransferEnvironment.Companion.ENV_QUEUE_URL
import software.amazon.awscdk.core.Construct
import software.amazon.awscdk.core.Duration
import software.amazon.awscdk.core.RemovalPolicy
import software.amazon.awscdk.core.Stack
import software.amazon.awscdk.services.apigateway.LambdaIntegration
import software.amazon.awscdk.services.apigateway.RestApi
import software.amazon.awscdk.services.iam.PolicyStatement
import software.amazon.awscdk.services.lambda.Code
import software.amazon.awscdk.services.lambda.Function
import software.amazon.awscdk.services.lambda.Runtime
import software.amazon.awscdk.services.lambda.eventsources.S3EventSource
import software.amazon.awscdk.services.lambda.eventsources.S3EventSourceProps
import software.amazon.awscdk.services.lambda.eventsources.SqsEventSource
import software.amazon.awscdk.services.lambda.eventsources.SqsEventSourceProps
import software.amazon.awscdk.services.s3.Bucket
import software.amazon.awscdk.services.s3.EventType
import software.amazon.awscdk.services.s3.NotificationKeyFilter
import software.amazon.awscdk.services.sqs.DeadLetterQueue
import software.amazon.awscdk.services.sqs.Queue

const val PO_QUEUE = "PoQueue"
const val PO_DLQ = "PoDLQ"

class PoTransferStack(scope: Construct?, id: String?, buildPath: String?) : Stack(scope, id, null) {

    companion object {
        const val S3_PO_BUCKET_NAME = "po-transfers"
    }

    init {
        /* The PO DLQ queue */
        val poDQLQueue = Queue.Builder.create(this, "PoDLQ")
                .queueName(PO_DLQ)
                .build()

        /* The PO queue */
        val poQueue = Queue.Builder.create(this, "PoQueue")
                .queueName(PO_QUEUE)
                .deadLetterQueue(DeadLetterQueue.builder()
                        .queue(poDQLQueue)
                        .maxReceiveCount(10)
                        .build())
                .build()

        /* The PO-poller, a Lambda function */
        val poPoller = Function.Builder.create(this, "PoUploadHandler")
                .functionName("PoUploadHandler")
                .runtime(Runtime.JAVA_11)
                .code(Code.fromAsset(buildPath!!))
                .handler("no.europris.po.lambda.PoUploadHandler::handleRequest")
                .memorySize(256)
                .timeout(Duration.seconds(30))
                .environment(mapOf(ENV_QUEUE_URL to poQueue.queueUrl))
                .build()

        /* The PO-sender, a Lambda function */
        val poSender = Function.Builder.create(this, "PoSender")
                .functionName("PoSender")
                .runtime(Runtime.JAVA_11)
                .code(Code.fromAsset(buildPath))
                .handler("no.europris.po.lambda.PoSender::handleRequest")
                .memorySize(512)
                .timeout(Duration.seconds(30))
                .environment(mapOf(ENV_QUEUE_URL to poQueue.queueUrl))
                .build()

        /* The PO-sender, a Lambda function */
        val poReceiver = Function.Builder.create(this, "PoReceiver")
                .functionName("PoReceiver")
                .runtime(Runtime.JAVA_11)
                .code(Code.fromAsset(buildPath))
                .handler("no.europris.po.lambda.PoReceiver::handleRequest")
                .memorySize(256)
                .timeout(Duration.seconds(30))
                .environment(mapOf(ENV_QUEUE_URL to poQueue.queueUrl))
                .build()

        /* The S3 bucket */
        val poTransfersBucket = Bucket.Builder.create(this, "PoTransfers")
                .versioned(true)
                //.blockPublicAccess(BlockPublicAccess.)
                //.accessControl(BucketAccessControl.AUTHENTICATED_READ) // TODO: this stuff
                .bucketName(S3_PO_BUCKET_NAME)
                .removalPolicy(RemovalPolicy.RETAIN)
                .build()


        /* Send event to lambda (poPoller) when new file is added to S3 bucket (poTransfersBucket) */
        poPoller.addEventSource(S3EventSource(poTransfersBucket, S3EventSourceProps.builder()
                .events(listOf(EventType.OBJECT_CREATED))
                .filters(listOf(NotificationKeyFilter.builder()
                        .prefix("outgoing/")
                        .suffix(".xml")
                        .build()))
                .build()
        ))

        /* Send event to lambda (poSender) when new message is ready for consumption from queue (poQueue) */
        poSender.addEventSource(SqsEventSource(poQueue, SqsEventSourceProps.builder()
                .batchSize(1)
                .build()
        ))

        /* API GW */
        val apiGw = RestApi.Builder.create(this, "po-transfer-api")
                .restApiName("po-transfer api")
                .description("This API responds to OMS sending PO and ASN messages.")
                .build()

        val omsResource = apiGw.root.addResource("oms")
        val orderResource = omsResource.addResource("order")
        val responseResource = orderResource.addResource("response")
        val asnResource = omsResource.addResource("asn")

        val poReceiverIntegration = LambdaIntegration.Builder.create(poReceiver)
                // .requestTemplates(mapOf()) // TODO: do we need to map this? Need actual XML to know
                .build()

        responseResource.addMethod("POST", poReceiverIntegration)
        asnResource.addMethod("POST", poReceiverIntegration)

        /* Give lambda (poPoller) permissions to fetch file from bucket (poTransfersBucket) */
        // poTransfersBucket.grantReadWrite(poPoller); // TODO: can replace the manual role policies below?
        val lambdaS3Policy = PolicyStatement.Builder.create()
                .actions(listOf("s3:GetObject"))
                .resources(listOf("*")) // TODO: tighten this
                .build()
        val lambdaSQSSendMessagePolicy = PolicyStatement.Builder.create()
                .actions(listOf("sqs:SendMessage"))
                .resources(listOf("*")) // TODO: tighten this
                .build()
        poPoller.addToRolePolicy(lambdaS3Policy)
        poPoller.addToRolePolicy(lambdaSQSSendMessagePolicy)
        val lambdaSQSReceiveAndDeletePolicy = PolicyStatement.Builder.create()
                .actions(listOf("sqs:ReceiveMessage", "sqs:DeleteMessage"))
                .resources(listOf("*")) // TODO: tighten this
                .build()
        poSender.addToRolePolicy(lambdaSQSReceiveAndDeletePolicy)
        poSender.addToRolePolicy(lambdaS3Policy)
        poSender.addToRolePolicy(lambdaSQSSendMessagePolicy)

        /* Give lambda (poReceiver) permission to put file into bucket (poTransfersBucket) */
        poReceiver.addToRolePolicy(PolicyStatement.Builder.create()
                .actions(listOf("s3:PutObject"))
                .resources(listOf("*")) // TODO: tighten this
                .build())
    }
}