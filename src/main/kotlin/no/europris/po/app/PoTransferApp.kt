package no.europris.po.app

import no.europris.po.pipeline.PoTransferPipelineStack
import software.amazon.awscdk.core.App
import software.amazon.awscdk.core.Environment
import software.amazon.awscdk.core.StackProps
import software.amazon.awssdk.regions.Region

object PoTransferApp {

    @JvmStatic
    fun main(args: Array<String>) {
        val app = App()

        PoTransferPipelineStack(app, "PoTransferPipelineStack",
                StackProps.Builder()
                        .stackName("PoTransferPipelineStack")
                        .env(
                                Environment.Builder()
                                        .account("477992598373") // TODO: parameterize ?
                                        .region(Region.EU_WEST_1.id())
                                        .build())
                        .build())

        app.synth()
    }

}