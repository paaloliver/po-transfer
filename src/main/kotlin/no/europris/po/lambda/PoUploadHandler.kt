package no.europris.po.lambda

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.S3Event
import com.amazonaws.services.sqs.AmazonSQSClientBuilder
import com.amazonaws.services.sqs.model.SendMessageRequest
import no.europris.po.lambda.helper.HandlerOutput
import no.europris.po.lambda.helper.PoTransferEnvironment.Companion.ENV_QUEUE_URL
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * This lambda receives S3 events and puts the incoming
 * S3 file key on a SQS queue
 */
class PoUploadHandler : RequestHandler<S3Event, HandlerOutput> {

    private val sqsClient = AmazonSQSClientBuilder.defaultClient()
    private val queueUrl = System.getenv(ENV_QUEUE_URL)
    private val log: Logger = LoggerFactory.getLogger(PoUploadHandler::class.java)

    override fun handleRequest(s3Event: S3Event?, context: Context?): HandlerOutput {
        log.debug("handleRequest() started")

        s3Event?.let {
            try {
                log.debug("s3Event records: ${s3Event.records.size}")
                val s3Record = it.records[0]
                val s3RecordKey = s3Record.s3.`object`.key
                log.info("Processing S3 object with key: $s3RecordKey")

                // put message about new file on sqs queue
                putMessageOnQueue(s3RecordKey)
                log.info("Message for $s3RecordKey is on queue.")

                log.debug("handleRequest() finished")
            } catch (e: Exception) {
                log.error("ERROR in PoUploadHandler", e)
            }
        }

        log.debug("handleRequest() END")
        return HandlerOutput("OK")
    }

    private fun putMessageOnQueue(s3RecordKey: String?) {
        log.debug("About to put message on SQS queue '$queueUrl' for S3 key: $s3RecordKey")

        val sendMessageRequest = SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(s3RecordKey)

        sqsClient.sendMessage(sendMessageRequest)
    }

}

