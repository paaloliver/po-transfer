package no.europris.po.lambda

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2HTTPEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2HTTPResponse
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import no.europris.po.app.PoTransferStack
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import software.amazon.awssdk.regions.Region
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class PoReceiver : RequestHandler<APIGatewayV2HTTPEvent, APIGatewayV2HTTPResponse> {

    private val log: Logger = LoggerFactory.getLogger(PoReceiver::class.java)

    private val s3Client: AmazonS3 = AmazonS3ClientBuilder.standard()
            .withRegion(Region.EU_WEST_1.id())
            .withForceGlobalBucketAccessEnabled(true)
            .build()


    override fun handleRequest(request: APIGatewayV2HTTPEvent?, context: Context?): APIGatewayV2HTTPResponse {

        log.debug("handleRequest() STARTING")
        log.debug("handleRequest() body: ${request?.body}")

        request?.let {
            // 1. Find type of incoming file
            val fileType = findFileType(request.body)
            var fileName = ""

            when (fileType) {
                POFileType.PURCHASE_ORDER_RESPONSE -> {
                    // 2. get either containerNumber or orderNumber from content
                    val orderNumber = findOrderNumber(request.body)
                    // 3. give file correct filename using (containerNumber/orderNumber)
                    fileName = generateOMSFileName(orderNumber, LocalDate.now())
                }
                POFileType.ADVANCED_SHIP_NOTICE -> {
                    // 2. get either containerNumber or orderNumber from content
                    val containerNumber = findContainerNumber(request.body)
                    // 3. give file correct filename using (containerNumber/orderNumber)
                    fileName = generateASNFileName(containerNumber, LocalDate.now())
                }
                else -> {
                    // unexpected file type
                    // TODO handle error break execution
                }
            }

            // 5. using S3 client, put file in bucket into the incoming folder
            s3Client.putObject(PoTransferStack.S3_PO_BUCKET_NAME, "incoming/$fileName", request.body)

            // - trap all failures and log with ERROR and create alert in LogWatch

            log.debug("handleRequest() FINISHED")
        }

        return APIGatewayV2HTTPResponse.builder()
                .withBody("ok")
                .withStatusCode(200)
                .build()

    }

    companion object {
        /**
         * Name format for oms: oms-<ordernumber>-<date>-<uuid>.xml
         */
        fun generateOMSFileName(orderNumber: String, date: LocalDate): String {
            return "oms-$orderNumber-${formatDateString(date)}-${getUUID()}.xml";
        }

        /**
         * Name format for asn: asn-<containernumber>-<date>-<uuid>.xml
         */
        fun generateASNFileName(containerNumber: String, date: LocalDate): String {
            return "asn-$containerNumber-${formatDateString(date)}-${getUUID()}.xml";
        }

        fun findFileType(body: String?): POFileType {
            // TODO implement parsing
            return POFileType.PURCHASE_ORDER_RESPONSE
        }

        fun findContainerNumber(body: String?): String {
            // TODO implement parsing
            return "7713"
        }

        private fun findOrderNumber(body: String?): String {
            // TODO implement parsing
            return "123987"
        }

        private fun formatDateString(date: LocalDate): String {
            return date.format(DateTimeFormatter.ofPattern("ddMMyyyy"))
        }

        private fun getUUID(): String {
            return UUID.randomUUID().toString()
        }

        enum class POFileType {
            PURCHASE_ORDER_RESPONSE,
            ADVANCED_SHIP_NOTICE
        }
    }

}