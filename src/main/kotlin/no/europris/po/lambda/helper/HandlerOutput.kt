package no.europris.po.lambda.helper

data class HandlerOutput(val message: String)