package no.europris.po.lambda.helper

class PoTransferEnvironment {

    companion object{
        const val ENV_QUEUE_URL = "poQueueUrl"
        const val ENV_OMS_PO_API_ENDPOINT = "po-oms-api-end-point"
    }

}