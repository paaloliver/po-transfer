package no.europris.po.lambda.helper

import software.amazon.awssdk.services.ssm.SsmClient
import software.amazon.awssdk.services.ssm.model.GetParameterRequest
import java.lang.RuntimeException

object ParameterStore {

    fun getParameter(ssmClient: SsmClient, parameterName: String) : String {
        try {
            val parameterRequest = GetParameterRequest.builder().name(parameterName).build()
            return ssmClient.getParameter(parameterRequest).parameter().value()
        }catch (e: Exception) {
            throw RuntimeException("Unable to get parameter '$parameterName'. Cause: ${e.cause}")
        }
    }

}