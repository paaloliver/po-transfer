package no.europris.po.lambda

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.SQSEvent
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.GetObjectRequest
import com.amazonaws.services.s3.model.S3ObjectInputStream
import com.amazonaws.services.sqs.AmazonSQSClientBuilder
import com.amazonaws.services.sqs.model.MessageSystemAttributeName
import com.github.kittinunf.fuel.httpPost
import no.europris.po.app.PoTransferStack
import no.europris.po.lambda.helper.HandlerOutput
import no.europris.po.lambda.helper.ParameterStore
import no.europris.po.lambda.helper.PoTransferEnvironment
import no.europris.po.lambda.helper.PoTransferEnvironment.Companion.ENV_QUEUE_URL
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.ssm.SsmClient
import kotlin.math.min
import kotlin.math.pow

/**
 * This lambda receives SQS messages with S3 keys, downloads the file
 * and uploads the file to the OMS http REST API.
 * If API fails, the lambda throws RuntimeException so the SQS can retry
 * the message after a set interval (visibility)
 */
class PoSender : RequestHandler<SQSEvent, HandlerOutput> {

    private val log: Logger = LoggerFactory.getLogger(PoSender::class.java)

    private var s3Client: AmazonS3 = AmazonS3ClientBuilder.standard()
           .withRegion(Region.EU_WEST_1.id())
           .withForceGlobalBucketAccessEnabled(true)
           .build()

    private val ssmClient: SsmClient = SsmClient.builder().region(Region.EU_WEST_1).build()

    private val poQueueUrl = System.getenv(ENV_QUEUE_URL)

    private val omsPoApiUrl by lazy{
        ParameterStore.getParameter(ssmClient, PoTransferEnvironment.ENV_OMS_PO_API_ENDPOINT)
    }

    private val sqsClient = AmazonSQSClientBuilder.defaultClient()

    override fun handleRequest(event: SQSEvent?, p1: Context?): HandlerOutput {
        log.debug("handleRequest() STARTING")

        event?.let {
            it.records.stream().forEach { sqsMessage ->
                log.info("Processing s3 object: ${sqsMessage.body}")
                val file = getFileFromS3(sqsMessage.body)
                sendFileToOms(file, sqsMessage, this::handleApiError)
                log.info("Successfully sent '${sqsMessage.body}' to OMS")
            }
        }

        return HandlerOutput("OK")
    }

    private fun handleApiError(sqsMessage: SQSEvent.SQSMessage, errorMessage: String) {
        log.error(errorMessage)
        log.warn("Resending message with key: ${sqsMessage.body}")
        changeMessageVisiblity(sqsMessage)
        throw RuntimeException("Error. Will retry in a while...")
    }

    private fun getFileFromS3(s3RecordKey: String): S3ObjectInputStream {
        log.debug("Attempting to get object with key '$s3RecordKey' from S3")
        val s3Object = s3Client.getObject(GetObjectRequest(PoTransferStack.S3_PO_BUCKET_NAME, s3RecordKey))
        return s3Object.objectContent
    }

    private fun sendFileToOms(objectDataStream: S3ObjectInputStream, sqsMessage: SQSEvent.SQSMessage, onErrorAction: (SQSEvent.SQSMessage, String) -> Unit) {
        try {
            log.debug("omsPoApiUrl: $omsPoApiUrl")
            val (request, response, result) = ("https://core-api-emea-int.damco.com/b2b/customers/primark/in/poupload").httpPost()
                    .body(objectDataStream.readAllBytes())
                    .response()

            val (payload, error) = result

            if (response.statusCode == 200) {
                payload?.let {
                    val responsePayload = String(payload)
                    log.info("API call successful. Response: $responsePayload")
                }
            } else {
                error?.let {
                    val errorResponseMessage = it.response.responseMessage
                    val errorMessage = "ERROR when sending file to API response: $errorResponseMessage"
                    onErrorAction(sqsMessage, errorMessage)
                }
            }
        } catch (e: Exception) {
            val errorMessage = "Unexpected error when sending file to API. Retrying... cause: ${e.cause}"
            onErrorAction(sqsMessage, errorMessage)
        }
    }

    /**
     * Called to make the retry message only visible for consumption after a given
     * interval (backoff). This is to increase the likelihood of the API being
     * able to respond properly and to avoid putting the message on the DLQ to quickly.
     */
    private fun changeMessageVisiblity(sqsMessage: SQSEvent.SQSMessage) {
        sqsClient.changeMessageVisibility(poQueueUrl, sqsMessage.receiptHandle, calculateVisibilityValue(sqsMessage))
    }

    private fun calculateVisibilityValue(sqsMessage: SQSEvent.SQSMessage): Int {
        val minimumWaitSeconds = 300 // 5 minutes
        val maximumWaitSeconds = 3600 // 1 hour

        var receiveCount = 1

        sqsMessage.attributes[MessageSystemAttributeName.ApproximateReceiveCount.toString()]?.let {
            receiveCount = it.toInt()
        }

        // Full jitter, ref: https://aws.amazon.com/blogs/architecture/exponential-backoff-and-jitter/
        val calculatedVisibiltyValue = (0 until min(maximumWaitSeconds, (minimumWaitSeconds * (2.0.pow(receiveCount))).toInt())).random()

        log.debug("approximateReceiveCount: $receiveCount")
        log.debug("calculatedVisibiltyValue: $calculatedVisibiltyValue")
        return calculatedVisibiltyValue;
    }

}
