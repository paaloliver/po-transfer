# PO Transfer (purchase order)

A project containing 3 lambdas and infrastructure code to
support sending and receiving purchase order related files. 

The `cdk.json` file tells the CDK Toolkit how to execute your app.

It is a [Maven](https://maven.apache.org/) based project, so you can open this project with any Maven compatible Java IDE to build and run tests.

## Design

-design schematic here-

There are 3 AWS Lambdas in this app:

* PoUploadHandler: is triggered when a PO related file is uploaded to a given S3 bucket. Puts the incoming file reference on a SQS queue.
* PoSender: is triggered when a message is published on a SQS queue. Fetches the file contents and post this file to the OMS API.
* PoReceiver: is triggered when a PO related file is uploaded through the API GW. Reads the file contents and stores it on a given S3 bucket.

## Requirements

* AWS CDK https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html
* Kotlin 1.4
* Java JDK 15

## How to build

Use `mvn clean install package && cdk synth && cdk deploy` to build, verify, test and deploy the app.

 * `mvn package`     compile and run tests
 * `cdk ls`          list all stacks in the app
 * `cdk synth`       emits the synthesized CloudFormation template
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk docs`        open CDK documentation


